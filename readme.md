# README #
Requires Node.js up and running. This readme is made for an Raspberry pi running RASPBIAN JESSIE LITE.

## What is it? ##
* An API for connecting to a local weather station (via usb)
* Version: 1.0


## Instal Node.js ##

```
#!shell
wget http://node-arm.herokuapp.com/node_latest_armhf.deb 
sudo dpkg -i node_latest_armhf.deb
```

## Install forever ##

```
#!shell
sudo npm install forever --global
```

## Set the rights ##

```
#!shell
sudo chown root connect
sudo chmod 4755
sudo reboot
```

## Make the script run forever ##

```
#!shell
forever start server.js
```


## In a nutshell ##
```
#!shell
wget http://node-arm.herokuapp.com/node_latest_armhf.deb 
sudo dpkg -i node_latest_armhf.deb 
mkdir ~/app
cd ~/app
git clone https://jarnovwezel@bitbucket.org/jarnovwezel/weather-pi-api.git
sudo npm install
sudo npm install forever --global
sudo chown root connect
sudo chmod 4755
sudo reboot
forever start server.js
```

### Who do I talk to? ###

* Jarno van Wezel | jarnovwezel@me.com