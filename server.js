require('console-stamp')(console, '[HH:MM:ss]');
require('shelljs/global');

var Mysql2 = require('mysql2');
var Tunnel = require('tunnel-ssh');
var Promise = require('bluebird');

var Weather = {
    //config
    config: {
        interval: 10, //in minutes
        ssh: {
            host: '',
            username: '',
            password: '',
            dstPort: 0,
            localPort: 0,
            dstHost: '',
            keepAlive: true
        },
        mysql: {
            host: 'jarn.io',
            user: 'weather',
            password: 'j=s=tRetab4w',
            database: 'weather',
            debug: false,
            timeout: 50000
        }
    },

    //defaults
    db: null,
    tunnel: null,
    data: [],
    interval: false,


    //functions
    init: function () {
        var args = process.argv.slice(2);

        if(args.indexOf("start") >= 0){
            console.log('starting loop...');

            if(args.indexOf("-i") >= 0){
                var param = args.indexOf("-i");
                Weather.config.interval = args[param + 1];
            }

            console.log('with interval ', Weather.config.interval, ' minute(s)');

            Weather.startInterval();

        }else if(args.indexOf("data") >= 0){
            Weather.getDataFromStation().then(function (result) {
                console.log(result);
                console.log(Weather.data);
            });
        }
        else{
            console.log('Canceled! Missing attribute start...');
        }
    },

    connectSSH: function () {
        return new Promise(function(resolve, reject) {
            if(Weather.tunnel == null) {

                Weather.tunnel = Tunnel(Weather.config.ssh, function (error, result) {
                    if (error) throw error;
                    resolve('ssh :: connected');
                });

                // Use a listener to handle errors outside the callback
                Weather.tunnel.on('error', function (err) {
                    console.error('Something bad happened:', err);
                });
            }else{
                resolve('ssh :: already connected');
            }
        });

    },

    connectDB: function() {
        return new Promise(function(resolve, reject) {
            if(Weather.db == null) {
                //create connection
                Weather.db = Mysql2.createConnection(Weather.config.mysql);

                //connect to our db
                Weather.db.connect(function (error) {
                    if (error) throw error;
                    resolve('db :: connected');
                });
            }else{
                resolve('db :: already connected');
            }
        });
    },

    disconnect: function(){
        Weather.tunnel.close();
        Weather.tunnel = null;

        Weather.db.end();
        Weather.db = null;
    },

    getDataFromStation: function(){
        return new Promise(function(resolve, reject) {
            console.log('data :: collecting');
            // get data from our weather station
            var shellData = exec("./connect -i0", {silent: true}).stdout;
            var tmp = shellData.split(':');

            /*
             *  0       : 1: 2: 3: 4: 5: 6: 7: 8: 9:10:11:12:   13:14:15:  16 :17:18:19:20:21
             *  Unixtime:T0:H0:T1:H1:T2:H2:T3:H3:T4:H4:T5:H5:PRESS:UV:FC:STORM:WD:WS:WG:WC:RC
             *
             -  Unixtime - Current date / time in a unixtime stamp format.
             -  T0    - temperature from internal sensor in °C
             -  H0    - humidity from internal sensor in % rel
             -  T1..5 - temperature from external sensor 1..4 in °C
             -  H1..5 - humidity from external sensor 1...4 in % rel
             -  PRESS - air pressure in mBar
             -  UV    - UV index from UV sensor
             -  FC    - station forecast, see below for more details
             -  STORM - stormwarning; 0 - no warning, 1 - fix your dog
             -  WD    - wind direction in n x 22.5°; 0 -> north
             -  WS    - wind speed in m/s
             -  WG    - wind gust speed in m/s
             -  WC    - windchill temperature in °C
             -  RC    - rain counter (maybe since station starts measurement) as value
             */

            // put it in an array
            Weather.data = {
                'timestamp': Number(tmp[0]),
                'T': [parseFloat(tmp[1])],
                'H': [parseFloat(tmp[2])],

                'PRESS': parseFloat(tmp[13]),
                'UV': parseFloat(tmp[14]),
                'FC': parseFloat(tmp[15]),
                'STORM': parseFloat(tmp[16]),
                'WD': parseFloat(tmp[17]),
                'WS': parseFloat(tmp[18]),
                'WG': parseFloat(tmp[19]),
                'WC': parseFloat(tmp[20]),
                'RC': parseFloat(tmp[21])
            };

            //3:4:5:6:7:8:9:10:11:12
            var sensors = 5;
            var start = 3;

            for (var i = 1; i <= sensors; i++) {
                Weather.data['T'].push(parseFloat(tmp[start]));
                start++;

                Weather.data['H'].push(parseFloat(tmp[start]));
                start++;
            }

            // convert it to json
            Weather.data['T'] = JSON.stringify(Weather.data['T']);
            Weather.data['H'] = JSON.stringify(Weather.data['H']);
            resolve('data :: collected');
        });
    },

    insertData: function(){
        return new Promise(function(resolve, reject) {
            if (Weather.data != null) {
                // try to insert
                Weather.db.query('INSERT INTO `data` SET ?', Weather.data, function (error, result) {
                    if (error) throw error;

                    //disconnect
                    Weather.disconnect();
                    resolve('db :: inserted ('+ result.insertId+')');
                });
            } else {
                resolve('db :: insert failed');
            }
        });
    },

    startInterval: function(){
        Weather.doTheChain();

        Weather.interval = setInterval(function () {
            Weather.doTheChain();
        }, Weather.config.interval * 60 * 1000);
    },

    doTheChain: function () {
        // Weather.connectSSH().then(function(result) {
            // console.log(result);
            Weather.connectDB().then(function (result) {

                console.log(result);
                Weather.getDataFromStation().then(function (result) {
                    console.log(result);
                    Weather.insertData().then(function (result) {

                        console.log(result);
                        return true;

                    }).catch(function() {
                        Weather.parseError('error');
                    });

                }).catch(function() {
                    Weather.parseError('error');
                });

            }).catch(function() {
                Weather.parseError('error');
            });

        // }).catch(function() {
        //     Weather.parseError('error');
        // });
    },

    parseError: function (error) {
        console.log(error);
    }
};

Weather.init();
